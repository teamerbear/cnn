"""
This module uses a simple neural network (Multi-layer Perceptron) with only a
single hidden layer to acheive good handwritten digit classification results.
"""

import numpy as np
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import np_utils

# Fix the random seed to a constant just to see that my results agree with the
# online tutorial.
my_seed = 7
np.random.seed(my_seed)

# ========== PREPARE DATA ======================================================

# Load the MNIST dataset (see the file 'example.py' for a visual representation
# of the data).
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Re-shape the 28 x 28 pixel matrix into a one dimensional matrix of length 784.
# Note that the data is organized in a three-dimensional numpy array:
#   [ sample index x horizontal pixel index x vertical pixel index]
# Each pixel is a grayscale value between 0 and 255.
num_pixels = x_train.shape[1] * x_train.shape[2]
x_train = x_train.reshape(x_train.shape[0], num_pixels).astype('float32')
x_test = x_test.reshape(x_test.shape[0], num_pixels).astype('float32')

# Normalize the inputs to the range [0, 1]
x_train = x_train / 255.0
x_test = x_test / 255.0

# Currently the labels are an integer (0 - 9) corresponding to what number the
# handwritten image actually represents. We wan't to change the format of the
# output to a 'one-hot encoded' vector. So, for example, the label '5' would be
# transformed into the vector [0, 0, 0, 0, 0, 1, 0, 0, 0, 0].
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)
num_classes = y_train.shape[1]

# ========== CREATE MODEL ======================================================


def baseline_model():
    # Create Keras model.
    model = Sequential()
    model.add(  # First (and only) hidden layer
        Dense(
            num_pixels,
            input_dim=num_pixels,
            kernel_initializer='normal',
            activation='relu'))
    model.add(  # Output layer
        Dense(
            num_classes, kernel_initializer='normal', activation='softmax'))

    # Compile the model.
    model.compile(
        loss='categorical_crossentropy',
        optimizer='adam',
        metrics=['accuracy'])

    return model


# ========== TRAIN THE MODEL ===================================================

model = baseline_model()
model.fit(x_train,
          y_train,
          validation_data=(x_test, y_test),
          epochs=10,
          batch_size=200,
          verbose=2)
scores = model.evaluate(x_test, y_test, verbose=0)
print "Baseline Error: {}".format(100.0 - scores[1] * 100.0)