"""
This script follows a tutorial on handwritten digit classification provided at:
    https://machinelearningmastery.com/handwritten-digit-recognition-using-convolutional-neural-networks-python-keras/
In the tutorial, I use a CNN to classify handwritten digits (0 - 9).
"""

from keras.datasets import mnist
import matplotlib.pyplot as plt

# Load (download if needed) the MNIST dataset.
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Plot four images as grayscale to see examples of the data we're dealing with.
# Each picture is a 28 x 28 handwritten image.
plt.subplot(221)
plt.imshow(x_train[0], cmap=plt.get_cmap('gray'))
plt.subplot(222)
plt.imshow(x_train[1], cmap=plt.get_cmap('gray'))
plt.subplot(223)
plt.imshow(x_train[2], cmap=plt.get_cmap('gray'))
plt.subplot(224)
plt.imshow(x_train[3], cmap=plt.get_cmap('gray'))
plt.show()