"""
This module builds up a simple Convolutional Neural Network (CNN) for the task
of handwritten digit classification.
"""
import numpy as np
from keras import backend as K
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.utils import np_utils

my_seed = 7
np.random.seed(my_seed)
K.set_image_dim_ordering('th')

# ========== PREPARE THE DATA ==================================================

# Load the data
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Re-shape the data so that it is of the form [samples][pixels][width][height].
#
# Here, [samples] is the index of the sample image within the dataset, [width]
# is the horizontal index of the pixel of interest, and [height] is the vertical
# index of the pixel of interest. The [pixels] depends on the type of image
# you're working with. In this case (the MNIST dataset), we're working with a
# grayscale image, so a pixel is described with just one value. For an RGB
# image, it takes three values to fully describe an image; there are essentially
# three arrays of pixels per image.
x_train = x_train.reshape(x_train.shape[0], 1, 28, 28).astype('float32')
x_test = x_test.reshape(x_test.shape[0], 1, 28, 28).astype('float32')

# We want to normalize the pixel values to the range [0, 1] instead of [0, 255].
x_train = x_train / 255.0
x_test = x_test / 255.0

# Transform the output labels into one-hot encoded vectors. For example, a label
# of '4' would correspond to the vector [0, 0, 0, 0, 1, 0, 0, 0, 0, 0].
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)
num_classes = y_test.shape[1]

# ========== DEFINE NN MODEL ===================================================


def baseline_model():
    # Create the model.
    model = Sequential()

    # Layer 1: A two-dimensional convolutional layer. This layer has 32 feature
    #          maps (TODO: what are those?). Each of these feature maps (TODO: I
    #          think this is right?) is 5x5 and uses the ReLU activation
    #          function.
    model.add(Conv2D(32, (5, 5), input_shape=(1, 28, 28), activation='relu'))

    # Layer 2: A max pooling layer with a pool size of 2x2. (TODO: What exactly
    #          is "max pooling"?)
    model.add(MaxPooling2D(pool_size=(2, 2)))

    # Layer 3:  A dropout layer. Here, we filter out 20% of the inputs to this
    #           layer.
    model.add(Dropout(0.2))

    # Layer 4: A flattening layer. The inputs to this layer are in 2-D matrix
    #          form. We want to convert them into a one-dimensional vector so
    #          that it can be used by the standard layers coming up.
    model.add(Flatten())

    # Layer 5: A standard fully connected layer with 128 neurons (TODO: Why 128?)
    #          that uses the ReLU activation function.
    model.add(Dense(128, activation='relu'))

    # Layer 6: The output prediction layer. This layer contains 10 neurons (one
    #          for each class 0-9) and uses the softmax activation function.
    model.add(Dense(num_classes, activation='softmax'))

    # Compile the model.
    model.compile(
        loss='categorical_crossentropy',
        optimizer='adam',
        metrics=['accuracy'])
    return model


# ========== TRAIN THE MODEL ===================================================

# Build and train the model. Note that this takes quite a while if you aren't
# running on a GPU. I am running on a MacBook Pro and it takes about 150 seconds
# per layer. This totals ~1500 seconds = 25 minutes.
model = baseline_model()
model.fit(x_train,
          y_train,
          validation_data=(x_test, y_test),
          epochs=10,
          batch_size=200,
          verbose=2)

# Evaluate the performance of the model.
scores = model.evaluate(x_test, y_test, verbose=0)
print "CNN Error: {}".format(100.0 - scores[1] * 100.0)
